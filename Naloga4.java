import java.io.*;

class BagElement {
    Object element;
    int quantity;
	BagElement next;

	BagElement() {
        element = null;
        quantity = 0;
		next = null;
	}
}

class Bag {

	private BagElement first;
	
	public Bag() {
		makenull();
	}
	
	public void makenull() {
		first = new BagElement();
	}
	
	public BagElement first() {
		return first;
	}   
	
	public BagElement next(BagElement pos) {
		return pos.next;
	}
	
	public void insert(Object obj, int q) {
		// nov element vstavimo samo, ce ga ni med obstojecimi elementi mnozice
		BagElement nov = new BagElement();
		nov.element = obj;
		nov.quantity = q;
		nov.next = null;
		boolean found = false;
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
			if ((int) obj < (int) retrieveElement(iter)) {
				if (iter == first()) {
					nov.next = first.next;
					first.next = nov;
				} else {
					nov.next = iter.next;
					iter.next = nov;
				}
				found = true;
				break;
			} else if (overEnd(iter.next) && (first().next != null)) {
				iter.next.next = nov;
				break;
			}
		}

		if (first().next == null) {
			nov.next = first.next;
			first.next = nov;
		}
	}
	
	public void delete(BagElement pos) {
		pos.next = pos.next.next;
	}
	
	public boolean overEnd(BagElement pos) {
		if (pos.next == null)
			return true;
		else
			return false;
	}
	
	public boolean empty() {
		return first.next == null;
	}
	
	public Object retrieveElement(BagElement pos) {
		return pos.next.element;
    }
    
    public int retrieveQuantity(BagElement pos) {
		return pos.next.quantity;
	}
	
	public BagElement locate(Object obj) {
		// sprehodimo se cez seznam elementov in preverimo enakost (uporabimo metodo equals)
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter))
			if (obj.equals(retrieveElement(iter))) {
				return iter;
            }
		return null;
	}
	
	/*public void print()	{
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
			System.out.print(retrieveElement(iter) + ":" + retrieveQuantity(iter));
			if (!overEnd(next(iter))) {
                System.out.print(",");
            }
		}
		System.out.println();
	}*/
	
	// ------------------- METODE -------------------
	
	// Ustvari novo vreco z elementi
    public void ustvari(int[][] elementi) {
        for (int i = 0; i < elementi.length; i++) {
            this.insert(elementi[i][0], elementi[i][1]);
        }
    }

    // zdruzi obe vreci, vse elemente vrece b dodaj vreci this
    public void zdruzi(Bag b) {
		for (BagElement iter = b.first(); !overEnd(iter); iter = b.next(iter)) {
			BagElement pos = this.locate(retrieveElement(iter));
			if (pos == null) {
				this.insert(retrieveElement(iter), retrieveQuantity(iter));
			} else {
				pos.next.quantity = pos.next.quantity + retrieveQuantity(iter);
			}
		}
    }

    // Iz vrece this odstrani vse elemente vrece b
    public void razlika(Bag b) {
		// POPRAVI! --> while loop!
		for (BagElement iter = b.first(); !overEnd(iter); iter = b.next(iter)) {
			BagElement pos = this.locate(retrieveElement(iter));
			if (pos != null) {
				pos.next.quantity = pos.next.quantity - retrieveQuantity(iter);
				if (pos.next.quantity <= 0) {
					delete(pos);
				}
			}
		}
    }

    // V vreci obdrzimo le elemente, ki so skupni v obeh vrecah (presek)  
    public void skupno(Bag b) {
		// POPRAVI! --> while loop!
		BagElement iter = first();
		while (!overEnd(iter)) {
			BagElement pos = b.locate(retrieveElement(iter));
			if (pos == null) {
				delete(iter);
			} else {
				if (iter.next.quantity > b.retrieveQuantity(pos)) {
					iter.next.quantity = b.retrieveQuantity(pos);
				}
				iter = next(iter);
			}
		}
    }

    // Stevilo pojavitev elementov v vreci omejimo na podano konstanto k
    public void porezi(int k) {
		BagElement iter = first();
		while (!overEnd(iter)) {
			if (k <= 0) {
				delete(iter);
			} else if (retrieveQuantity(iter) > k) {
				iter.next.quantity = k;
				iter = next(iter);
			} else {
				iter = next(iter);
			}
		}
    }

    // V vreci ohranimo samo elemente, ki se v njej pojavijo vsaj tolikokrat kot je vrednost konstante k
    public void obdrzi(int k) {
		BagElement iter = first();
		while (!overEnd(iter)) {
			if (!(retrieveQuantity(iter) >= k)) {
				delete(iter);
			} else {
				iter = next(iter);
			}
		}
    }

	// Izpisi vreco v file
    public void izpisi(BufferedWriter bw) {
		try {
			for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
				bw.write(retrieveElement(iter) + ":" + retrieveQuantity(iter));
				if (!overEnd(next(iter))) {
					bw.write(",");
				}
			}
			bw.write("\n");
		} catch (IOException ex) {
			System.out.println("Something went wrong while writing file");
        }
    }
}

public class Naloga4 {
    public static void main(String[] args) {
		String input = args[0];//"Testi/I4_10.txt"; //args[0]; 
		String output = args[1];//"rez.txt"; //args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);
			
			Bag[] map = new Bag[10000]; // Preslikava ????
			String[] line;
            String[] subline;
			int[][] bag_items;
			
			//------------------- Read file --------------
			// Read n
			int n = Integer.valueOf(br.readLine());
			
			// Read commands
			for (int i = 0; i < n; i++) {
				line = br.readLine().split(",");
				switch (line[0].charAt(0)) {
					case 'U':
						Bag a = new Bag();
						bag_items = new int[line.length - 2][2];
						for (int j = 2; j < line.length; j++) {
							subline = line[j].split(":");
							bag_items[j - 2][0] = Integer.valueOf(subline[0]);
							bag_items[j - 2][1] = Integer.valueOf(subline[1]);
						}
						a.ustvari(bag_items);
						map[Integer.valueOf(line[1])] = a;
						//a.print();
						break;
					case 'Z':
						map[Integer.valueOf(line[1])].zdruzi(map[Integer.valueOf(line[2])]);
						break;
					case 'R':
						map[Integer.valueOf(line[1])].razlika(map[Integer.valueOf(line[2])]);
						break;
					case 'S':
						map[Integer.valueOf(line[1])].skupno(map[Integer.valueOf(line[2])]);
						break;
					case 'P':
						map[Integer.valueOf(line[1])].porezi(Integer.valueOf(line[2]));
						break;
					case 'O':
						map[Integer.valueOf(line[1])].obdrzi(Integer.valueOf(line[2]));
						break;
					case 'I':
						map[Integer.valueOf(line[1])].izpisi(bw);
						break;
				}
				
			}

			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
            
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading/writing file");
        }
        
	}
}