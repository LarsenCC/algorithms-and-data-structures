import java.io.*;

class Node {
    int min;
    int max;
    Node first;
    Node second;
    Node third;
    Node forth;

    public Node(int min, int max) {
        this.min = min;
        this.max = max;
        this.first = null;
        this.second = null;
        this.third = null;
        this.forth = null;
    }

    public Node() {
        this.min = 0;
        this.max = 0;
        this.first = null;
        this.second = null;
        this.third = null;
        this.forth = null;
    }
}

class MapTree {
    Node root;
    int flood_calls;
    
    public MapTree() {
        makenull();
    }

    void makenull() {
        root = null;
    }

    public Node buildTree(int[][] array, int i1, int j1, int i2, int j2) {
        if (i1 == i2 && j1 == j2) {
            Node n = new Node(array[i1][j1], array[i1][j1]);
            return n;
        }

        Node n = new Node();
        n.first = buildTree(array, i1, j1, (i2 + i1) / 2, (j2 + j1) / 2);
        n.second = buildTree(array, ((i2 + i1) / 2) + 1, j1, i2, (j2 + j1) / 2);
        n.third = buildTree(array, i1, ((j2 + j1) / 2) + 1, (i2 + i1) / 2, j2);
        n.forth = buildTree(array, ((i2 + i1) / 2) + 1, ((j2 + j1) / 2) + 1, i2, j2);

        n.min = min(n.first.min, n.second.min, n.third.min, n.forth.min);
        n.max = max(n.first.max, n.second.max, n.third.max, n.forth.max);

        if (n.min == n.max) {
            n.first = null;
            n.second = null;
            n.third = null;
            n.forth = null;
        }

        return n;
    }

    private int min(int i1, int i2, int i3, int i4) {
        return Math.min(i1, Math.min(i2, Math.min(i3, i4)));
    }

    private int max(int i1, int i2, int i3, int i4) {
        return Math.max(i1, Math.max(i2, Math.max(i3, i4)));
    }

    public int searchFloodedAreas(int map_size, int height, Node n) {
        flood_calls++;

        if (n.max == n.min) {
            if (n.max <= height) {
                return map_size * map_size;
            } else {
                return 0;
            }
        }

        if (n.min > height) {
            return 0;
        }

        if (n.max <= height) {
            return map_size * map_size;
        }

        int first = searchFloodedAreas(map_size / 2, height, n.first);
        int second = searchFloodedAreas(map_size / 2, height, n.second);
        int third = searchFloodedAreas(map_size / 2, height, n.third);
        int forth = searchFloodedAreas(map_size / 2, height, n.forth);

        return first + second + third + forth;
    }
}

public class Naloga8 {

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        String input = args[0];
		String output = args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
            BufferedWriter bw = new BufferedWriter(fw);

            //CODE
            int map_size = Integer.valueOf(br.readLine());
            int[][] map = new int[map_size][map_size];

            for (int i = 0; i < map_size; i++) {
                String[] line = br.readLine().split(",");
                for (int j = 0; j < map_size; j++) {
                    map[i][j] = Integer.valueOf(line[j]);
                }
            }

            MapTree mt = new MapTree();
            mt.root = mt.buildTree(map, 0, 0, map_size - 1, map_size - 1);

            int num = Integer.valueOf(br.readLine());

            for (int i = 0; i < num; i++) {
                mt.flood_calls = 0;
                int flooded_area = mt.searchFloodedAreas(map_size, Integer.valueOf(br.readLine()), mt.root);
                //System.out.println("Flooded area: " + flooded_area + ", calls: " + mt.flood_calls);
                bw.write(flooded_area + "," + mt.flood_calls + "\n");
            }

            // Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
            
            System.out.println("Time: " + (System.currentTimeMillis() - time));
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
    }
}