import java.io.*;

public class Naloga1 {

	// CHOOSE MEDIANA --> for quicksort algorithm
	public static int chosePivot(String[] array, int i1, int i2) {
        int first = array[i1].length();
        int last = array[i2].length();
        int middle = array[(i1 + i2) / 2].length();
        if (i2 - i1 >= 2) {
            if ((last > first) && (first > middle) || (middle > first) && (first > last)) {
                //System.out.println("MEDIANA IS FIRST");
                return i1;
            }
            if ((last < first) && (last > middle) || (middle > last) && (last > first)) {
                //System.out.println("MEDIANA IS LAST");
                return i2;
            }
            if ((last > middle) && (middle > first) || (middle < first) && (middle > last)) {
                //System.out.println("MEDIANA IS MIDDLE");
                return (i1 + i2) / 2;
            }
        }

        return i1;
    }

	// QUICKSORT
    public static String[] quicksort(String[] array, int i1, int i2) {

        if (i2 - i1 < 1) {
            return array;
        }

        int pivot = chosePivot(array, i1, i2);

        //PARTITION 
        int smaller = i1;
        int bigger = i1;

        String temp;
        temp = array[i1];
        array[i1] = array[pivot];
        array[pivot] = temp;

        for (int i = i1; i <= i2; i++) {
            if (array[i].length() > array[i1].length()) {
                temp = array[smaller + 1];
                array[smaller + 1] = array[i];
                array[i] = temp;
                smaller++;
                bigger++;
            } else if (array[i].length() <= array[i1].length()) {
                if (bigger == i1) {
                    bigger = i;
                } else {
                    bigger++;
                }
            }
        }

        temp = array[smaller];
        array[smaller] = array[i1];
        array[i1] = temp;

        String[] firstHalf = quicksort(array, i1, smaller - 1);
        return quicksort(firstHalf, smaller + 1, bigger);
    }

	//---------------- beginnig NALOGA 1 -------------------
	static int[] coordinates;
	static String[] words;

	// Cover all directions
	static int[] dir_i = {0, 1, 0, -1, 1, 1, -1, -1};
	static int[] dir_j = {1, 0, -1, 0, 1, -1, -1, 1};

	// Print current table --> for debuggin purposes
	/*public static void printTable(char[][] table) {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
		for (int i = 0; i < table.length; i++) {
			System.out.print("- ");
		}
		System.out.println();
	}*/

	// Put characters/spaces in table
	public static void put(int wordsIndex, int coordsIndex, char[][] table, boolean correct, int dir) {
		int i1 = coordinates[coordsIndex + 0];
		int j1 = coordinates[coordsIndex + 1];

		for (int i = 0; i < words[wordsIndex].length(); i++) {
			table[i1 + i*dir_i[dir]][j1 + i*dir_j[dir]] = correct ? ' ': words[wordsIndex].charAt(i);
		}
	}
	
	public static boolean solve(char[][] table, int wordsIndex, int coordsIndex) {
		if (wordsIndex == words.length) {
            return true;
		}
		
       	for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				if (table[i][j] != ' ') {
					if (table[i][j] == words[wordsIndex].charAt(0)) {
						for (int dir = 0; dir < 8; dir++) {
							boolean found = true;
							try {
								for (int l = 1; l < words[wordsIndex].length(); l++) {
									if (table[i + l*dir_i[dir]][j + l*dir_j[dir]] != words[wordsIndex].charAt(l)) {
										found = false;
										break;
									}
								}

								if (found) {
									coordinates[coordsIndex] = i;
									coordinates[coordsIndex + 1] = j;
									coordinates[coordsIndex + 2] = i + dir_i[dir]*(words[wordsIndex].length() - 1);
									coordinates[coordsIndex + 3] = j + dir_j[dir]*(words[wordsIndex].length() - 1);
	
									put(wordsIndex, coordsIndex, table, true, dir);
									//printTable(table); //--> for debugging purposes
									if (solve(table, wordsIndex + 1, coordsIndex + 4)) {
										return true;
									} else {
										put(wordsIndex, coordsIndex, table, false, dir);
									}
								}
							} catch (Exception e) {/*catch OutOfBoundsException ...*/}
						}
					}
				}
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		String input = args[0]; 
		String output = args[1];
		
		int rows, columns;
		char[][] table;
		int countWords;
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
			BufferedReader br = new BufferedReader(fr);
			
			// Create File and Buffered writer for output files
			FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);

			//------------------- Read file --------------
			String[] split = br.readLine().split(",");
 			rows = Integer.valueOf(split[0]);
			columns = Integer.valueOf(split[1]);
			table = new char[rows][columns];
			
			// Fill table with letters
			for (int i = 0; i < rows; i++) {
				split = br.readLine().split(",");
				for (int j = 0; j < columns; j++) {
					table[i][j] = split[j].charAt(0);
				}
			}
			
			// Read words
			countWords = Integer.valueOf(br.readLine());
			words = new String[countWords];
			coordinates = new int[4 * countWords];
			
			for (int i = 0; i < countWords; i++) {
				words[i] = br.readLine();
			} 
			//------------------- DONE reading file -------------
			
			// Sort --> from longest to shortest String (this makes it faster ... by a lot)
			words = quicksort(words, 0, words.length - 1);

			// Solve --> recursive function
			solve(table, 0, 0);
			
			// Write output
			for (int i = 0; i < coordinates.length; i++) {
				if (i % 4 == 0) {
					bw.write(String.valueOf(words[i / 4]) + ",");
				}

				if (i % 4 == 3) {
					bw.write(String.valueOf(coordinates[i]) + "\n");
				} else {
					bw.write(String.valueOf(coordinates[i]) + ",");
				}
			}

			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
			
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading/writing file");
		}
	}
}
