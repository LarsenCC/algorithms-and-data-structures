import java.io.*;

class LinkedListElement {
	Object element;
	LinkedListElement next;
	
	LinkedListElement(Object obj) {
		element = obj;
		next = null;
	}
	
	LinkedListElement(Object obj, LinkedListElement nxt) {
		element = obj;
		next = nxt;
	}
}


class LinkedList {
	protected LinkedListElement first;
	protected LinkedListElement last;
	
	LinkedList() {
		makenull();
	}
	
	//Funkcija makenull inicializira seznam
	public void makenull() {
		//drzimo se implementacije iz ucbenika:
		//po dogovoru je na zacetku glava seznama (header)
		first = new LinkedListElement(null, null);
		last = null;
	}
	
	//Funkcija addLast doda nov element na konec seznama
	public void addLast(Object obj) {
		//najprej naredimo nov element
		LinkedListElement newEl = new LinkedListElement(obj, null);
		
		//ali je seznam prazen?
		// po dogovoru velja: ce je seznam prazen, potem kazalec "last" ne kaze nikamor
		if (last == null) {
			//ce seznam vsebuje samo en element, kazalca "first" in "last" kazeta na glavo seznama
			first.next = newEl;
			last = first;
		} else {
			last.next.next = newEl;
			last = last.next;
		}
	}
	
	//Funkcija write izpise elemente seznama
	/*public void write() {

		LinkedListElement el;
		
		//zacnemo pri elementu za glavo seznama
		el = first.next;
		while (el != null) {
			if ((last != null) && el != last.next) {
				System.out.print(String.valueOf(el.element) + ",");
			} else {
				System.out.print(String.valueOf(el.element));
			}
			
			el = el.next;
		}
		
		System.out.println();
		
	}*/

	// Funkcija, ki naredi string iz elemntov
	public void writeString(BufferedWriter bw) {

		LinkedListElement el;

		//zacnemo pri elementu za glavo seznama
		el = first.next;
		try {
			while (el != null) {
				if ((last != null) && el != last.next) {
					bw.write(String.valueOf(el.element) + ",");
				} else {
					bw.write(String.valueOf(el.element));
				}
				
				el = el.next;
			}

			bw.write("\n");
		} catch (IOException ex) {
			System.out.println("Something went wrong while writing file");
		}
	}
	
	//Funkcija addFirst doda nov element na prvo mesto v seznamu (takoj za glavo seznama)
	void addFirst(Object obj) {
		//najprej naredimo nov element
		LinkedListElement newEl = new LinkedListElement(obj);
		
		//novi element postavimo za glavo seznama
		newEl.next = first.next;
		first.next = newEl;
		
		if (last == null)//preverimo, ali je to edini element v seznamu
			last = first;
		else if (last == first)//preverimo, ali je seznam vseboval en sam element
			last = newEl;
	}
	
    public void preslikaj(char op, int val) {

		LinkedListElement el;
		el = first.next;

		while (el != null) {
			switch(op) {
				case '+':
					el.element = (int) el.element + val;
					break;
				case '*':
					el.element = (int) el.element * val;
			}

			el = el.next;
		}
    }

    public void ohrani(char op, int val) {
		LinkedListElement el, prev;
		el = first;
		prev = null;
		
		while (el.next != null) {
			boolean kill = true;

			switch(op) {
				case '<':
					if ((int) el.next.element < val) { kill = false; }
					break;
				case '>':
					if ((int) el.next.element > val) { kill = false; }
					break;
				case '=':
					if ((int) el.next.element == val) { kill = false; }
					break;
			}

			if (kill) {
				if (el.next == last) {
					el.next = last.next;
					last = el;
				} else if (el == last) {
					el.next = null;
					last = prev;
				}else {
					el.next = el.next.next;
				}
			} else {
				prev = el;
				el = el.next;
			}
		}
    }

    public void zdruzi(char op) {
		LinkedListElement el;
		el = first.next;
		int rez = 0;
		if (op == '*') { rez = 1; }

		while (el != null) {
			switch(op) {
				case '+':
					rez += (int) el.element;
					break;
				case '*':
					rez *= (int) el.element;
			}

			el = el.next;
		}

		LinkedListElement r = new LinkedListElement(rez);
		r.next = null;
		this.last = null;
		this.first.next = r;
    }
}

public class Naloga3 {
    
    public static void main(String[] args) {
		String input = args[0];
		String output = args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader
			FileReader fr = new FileReader(input);
			BufferedReader br = new BufferedReader(fr);
			
			// Create File and Buffered writer for output files
			FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);

			// Variables that store input data
			LinkedList list = new LinkedList();
			char[] ukaz, oper;
			int[] vrednosti;

			//------------------- Read file --------------
			String[] els = br.readLine().split(",");

			for (int i = 0; i < els.length; i++) {
				list.addLast(Integer.valueOf(els[i]));
			}

			int index = Integer.valueOf(br.readLine());

			ukaz = new char[index];
			oper = new char[index];
			vrednosti = new int[index];

			//long time_start = System.currentTimeMillis(); // --> casovna zahtevnost
			
			for (int i = 0; i < index; i++) {
				String[] line = br.readLine().split(",");
				ukaz[i] = line[0].charAt(0);
				oper[i] = line[1].charAt(0);
				if (ukaz[i] != 'z') {
					vrednosti[i] = Integer.valueOf(line[2]);
				}

				switch(ukaz[i]) {
					case 'o':
						list.ohrani(oper[i], vrednosti[i]);
						//list.write();
						break;
					case 'p':
						list.preslikaj(oper[i], vrednosti[i]);
						//list.write();
						break;
					case 'z':
						list.zdruzi(oper[i]);
						//list.write();
						break;
				}

				list.writeString(bw);
			}

			//System.out.println("Time: " + ((long) System.currentTimeMillis() - time_start) + " ms"); // --> casovna zahtevnost
			//------------------- DONE reading file -------------

			// Always close the bufferedReader and bufferedWriter!
			br.close();
			bw.close();
			
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading or writing file");
		}
		
	}
}