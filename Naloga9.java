import java.io.*;
import java.util.*;

class BagElement {
    int element;
	BagElement next;

	BagElement() {
        element = 0;
		next = null;
	}
}

class Bag {

	private BagElement first;
	
	public Bag() {
		makenull();
	}
	
	public void makenull() {
		first = new BagElement();
	}
	
	public BagElement first() {
		return first;
	}   
	
	public BagElement next(BagElement pos) {
		return pos.next;
	}
	
	public void insert(int obj) {
		// nov element vstavimo samo, ce ga ni med obstojecimi elementi mnozice
		BagElement nov = new BagElement();
		nov.element = obj;
		nov.next = null;
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
			if ((int) obj < (int) retrieveElement(iter)) {
				if (iter == first()) {
					nov.next = first.next;
					first.next = nov;
				} else {
					nov.next = iter.next;
					iter.next = nov;
				}
				break;
			} else if (overEnd(iter.next) && (first().next != null)) {
				iter.next.next = nov;
				break;
			}
		}

		if (first().next == null) {
			nov.next = first.next;
			first.next = nov;
		}
	}
	
	public void delete(BagElement pos) {
		pos.next = pos.next.next;
	}
	
	public boolean overEnd(BagElement pos) {
		if (pos.next == null)
			return true;
		else
			return false;
	}
	
	public boolean empty() {
		return first.next == null;
	}
	
	public int retrieveElement(BagElement pos) {
		return pos.next.element;
    }
    
	public BagElement locate(Object obj) {
		// sprehodimo se cez seznam elementov in preverimo enakost (uporabimo metodo equals)
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter))
			if (obj.equals(retrieveElement(iter))) {
				return iter;
            }
		return null;
	}
	
	public void print()	{
		for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
			System.out.print(String.valueOf(retrieveElement(iter)));
			if (!overEnd(next(iter))) {
                System.out.print(",");
            }
		}
		System.out.println();
	}
	
	// ------------------- METODE -------------------

    // zdruzi obe vreci, vse elemente vrece b dodaj vreci this
    public void zdruzi(Bag b) {
		for (BagElement iter = b.first(); !overEnd(iter); iter = b.next(iter)) {
			BagElement pos = this.locate(retrieveElement(iter));
			if (pos == null) {
				this.insert(retrieveElement(iter));
			}
		}
    }

	// Izpisi vreco v file
    public void izpisi(BufferedWriter bw) {
		try {
			for (BagElement iter = first(); !overEnd(iter); iter = next(iter)) {
				bw.write(String.valueOf(retrieveElement(iter)));
				if (!overEnd(next(iter))) {
					bw.write(",");
				}
			}
			bw.write("\n");
		} catch (IOException ex) {
			System.out.println("Something went wrong while writing file");
        }
    }
}

public class Naloga9 {

    // CHOOSE PIVOT for QuickSort
    public static int chosePivot(double[][] array, int i1, int i2) {
        double first = array[i1][0];
        double last = array[i2][0];
        double middle = array[(i1 + i2) / 2][0];
        if (i2 - i1 >= 2) {
            if ((last > first) && (first > middle) || (middle > first) && (first > last)) {
                //System.out.println("MEDIANA IS FIRST");
                return i1;
            }
            if ((last < first) && (last > middle) || (middle > last) && (last > first)) {
                //System.out.println("MEDIANA IS LAST");
                return i2;
            }
            if ((last > middle) && (middle > first) || (middle < first) && (middle > last)) {
                //System.out.println("MEDIANA IS MIDDLE");
                return (i1 + i2) / 2;
            }
        }

        return i1;
    }

    // QUICKSORT
    public static double[][] quicksort(double[][] array, int i1, int i2) {
        //System.out.println(Arrays.toString(array));

        if (i2 - i1 < 1) {
            return array;
        }

        //CHOOSE PIVOT
        //int pivot = i1; // first element
        //int pivot = i2; // last element
        int pivot = chosePivot(array, i1, i2); // mediane of three
        //System.out.printf("PIVOT IS: %3d \nindex1: %3d index2: %3d\n\n", array[pivot], i1, i2);

        //PARTITION 
        int manjsi = i1;
        int vecji = i1;

        double temp0, temp1, temp2;
        temp0 = array[i1][0];
        temp1 = array[i1][1];
        temp2 = array[i1][2];
        array[i1][0] = array[pivot][0];
        array[i1][1] = array[pivot][1];
        array[i1][2] = array[pivot][2];
        array[pivot][0] = temp0;
        array[pivot][1] = temp1;
        array[pivot][2] = temp2;

        for (int i = i1; i <= i2; i++) {
            if (array[i][0] < array[i1][0]) {
                temp0 = array[manjsi + 1][0];
                temp1 = array[manjsi + 1][1];
                temp2 = array[manjsi + 1][2];
                array[manjsi + 1][0] = array[i][0];
                array[manjsi + 1][1] = array[i][1];
                array[manjsi + 1][2] = array[i][2];
                array[i][0] = temp0;
                array[i][1] = temp1;
                array[i][2] = temp2;
                manjsi++;
                vecji++;
            } else if (array[i][0] >= array[i1][0]) {
                if (vecji == i1) {
                    vecji = i;
                } else {
                    vecji++;
                }
            }
        }

        temp0 = array[manjsi][0];
        temp1 = array[manjsi][1];
        temp2 = array[manjsi][2];
        array[manjsi][0] = array[i1][0];
        array[manjsi][1] = array[i1][1];
        array[manjsi][2] = array[i1][2];
        array[i1][0] = temp0;
        array[i1][1] = temp1;
        array[i1][2] = temp2;

        //System.out.println(Arrays.toString(array));
        //--------------------------------------
        double[][] firstHalf = quicksort(array, i1, manjsi - 1);
        return quicksort(firstHalf, manjsi + 1, vecji);
    }

    public static void main(String[] args) {
        long time = System.currentTimeMillis();

        String input =  "Testi/I_2.txt"; //"Testi/I_2.txt";
		String output = "rez.txt"; //"rez.txt";
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);			
            
            // CODE
            int num_of_data = Integer.valueOf(br.readLine());
            double[][] coords = new double[num_of_data][2];
            boolean[] clustered = new boolean[num_of_data];

            LinkedList<Bag> clusters = new LinkedList<Bag>();

            for (int i = 0; i < num_of_data; i++) {
                Bag b = new Bag();
                b.insert(i + 1);
                clusters.add(b);

                clustered[i] = false;

                String[] line = br.readLine().split(",");
                coords[i][0] = Double.parseDouble(line[0]);
                coords[i][1] = Double.parseDouble(line[1]);
                //System.out.println(coords[i][0]);
            }

            int num_of_clusters = Integer.valueOf(br.readLine());
            int current_num_of_clusters = num_of_data;
            
            double[][] distances = new double[((num_of_data - 1) * num_of_data) / 2][3];
            int index = 0;

            

            for (int i = 0; i < num_of_data - 1; i++) {
                for (int j = i + 1; j < num_of_data; j++) {
                    distances[index][0] = euclideanDistance(coords[i][0], coords[i][1], coords[j][0], coords[j][1]);
                    distances[index][1] = i + 1;
                    distances[index][2] = j + 1;
                    index++;
                }
            }

            /*long time_read_and_sort = System.currentTimeMillis();

            // Override sort function for comparing to match our needs ... and sort the distances array
            Arrays.sort(distances, new java.util.Comparator<double[]>() {
                @Override
                public int compare(double[] a, double[] b) {
                    // sort via index 0
                    return Double.compare(a[0], b[0]);
                }
            });

            System.out.println("Sort time: " + (System.currentTimeMillis() - time_read_and_sort)); */

            long time_read_and_sort = System.currentTimeMillis();

            //double[][] quick_sort_array = quicksort(distances, 0, distances.length - 1);
            quicksort(distances, 0, distances.length - 1);

            System.out.println("Sort time: " + (System.currentTimeMillis() - time_read_and_sort));

            for (int i = 0; i < distances.length; i++) {

                // PRINTANJE ZASE
                /*for (int k = 0; k < current_num_of_clusters; k++) {
                    clusters.get(k).print();
                }

                System.out.println("------------------");*/

                int num1 = (int) distances[i][1];
                int num2 = (int) distances[i][2];

                /*if (clustered[num1 - 1] && clustered[num2 - 1]) {
                    continue;
                }*/

                Bag first = null;
                Bag second = null;
                int first_bag_index = 0;
                int second_bag_index = 0;

                for (int j = 0; j < current_num_of_clusters; j++) {
                    Bag temp = clusters.get(j);

                    if (first == null && temp.locate(num1) != null) {
                        first = temp;
                        first_bag_index = j;
                    }

                    if (second == null && temp.locate(num2) != null) {
                        second = temp;
                        second_bag_index = j;
                    }

                    if (second != null && first != null) {
                        break;
                    }
                }

                if (first == second){
                    continue;
                } else {
                    if (first.retrieveElement(first.first()) < second.retrieveElement(second.first())) {
                        first.zdruzi(second);
                        clusters.remove(second_bag_index);
                    } else {
                        second.zdruzi(first);
                        clusters.remove(first_bag_index);
                    }

                    current_num_of_clusters--;
                    clustered[num1 - 1] = true;
                    clustered[num2 - 1] = true;
                }

                if (num_of_clusters == current_num_of_clusters) {
                    break;
                }
            }

            // print the result
            for (int i = 0; i < num_of_clusters; i++) {
                clusters.get(i).izpisi(bw);
            }

            // PRINTANJE ZASE
            for (int k = 0; k < current_num_of_clusters; k++) {
                clusters.get(k).print();
            }
            
            System.out.println("------------------");

			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
            
            System.out.println("Time: " + (System.currentTimeMillis() - time));

		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
    }

    public static double euclideanDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow((x2 - x1), 2.0) + Math.pow((y2 - y1), 2.0));
    }
}