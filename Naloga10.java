import java.io.*;
import java.util.*;

class Node {
    int value;
    Node left;
    Node right;

    public Node(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

}

class BinaryTree {
    Node root;

    public BinaryTree() {
        makenull();
    }

    void makenull() {
        root = null;
    }

    public Node insertInorder(String[] list) {
        if (list.length <= 0) {
            return null;
        }

        int max = -1;
        int indexMax = 0;
        for (int i = 0; i < list.length; i++) {
            int el = Integer.valueOf(list[i]);
            if (max < el) {
                max = el;
                indexMax = i;
            }
        }

        Node node = null;

        if (max != -1) {
            node = new Node(max);
            if (root == null)
                root = node;   

            String[] left = subArray(list, 0, indexMax);
            String[] right = subArray(list, indexMax + 1, list.length);

            node.left = insertInorder(left);
            node.right = insertInorder(right);
        }

        return node;
    }

    private String[] subArray(String[] array, int beg, int end) {
        int len = end - beg;
        String[] arr = new String[len];

        for (int i = 0; i < len; i++) {
            arr[i] = array[i + beg];
        }

        return arr;
    }

    public void printLevel(Node root, BufferedWriter bw) { 
        int h = height(root);
        boolean comma = false;
        for (int i = 1; i <= h; i++) {
            try {
                bw.write(printGivenLevel(root, i, comma));
            } catch (IOException e){}
            comma = true;
        }
    }
    
    private String printGivenLevel(Node root, int level, boolean comma) { 
        if (root == null)
            return "";
        if (level == 1) {
            if (comma)
                return String.valueOf("," + root.value);
            else
                return String.valueOf(root.value);
        } else {
            return printGivenLevel(root.left, level - 1, comma) + printGivenLevel(root.right, level - 1, comma);
        }
    } 

    private int height(Node node) {
		if (node == null)
			return 0;
		else
			return Math.max(height(node.left), height(node.right)) + 1;
	}
}

public class Naloga10 {

    public static void main(String[] args) {
        long time = System.currentTimeMillis();
        String input = args[0]; 
		String output = args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);
			
            String[] line = br.readLine().split(",");
            BinaryTree bt = new BinaryTree();

            bt.insertInorder(line);
            bt.printLevel(bt.root, bw);
            
			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
            
            System.out.println("Time: " + (System.currentTimeMillis() - time));
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
    }

}