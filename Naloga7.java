import java.io.*;
import java.util.*;

public class Naloga7 {

	static HashSet<Integer> path = new HashSet<>();
	//static SortedSet<Integer> path = new TreeSet<>();
	static HashSet<String> all_paths = new HashSet<String>();
	
	public static void findPaths(double[][] adj_matrix, int index, int end_vertex, double budget, boolean end) {

		if (budget < 0.0) return;

		if (end && index == end_vertex) {
			//System.out.println(path.toString());
			
			all_paths.add((new TreeSet<>(path)).toString());
			//all_paths.add(getString(path));
			return;
		}

		for (int i = 0; i < adj_matrix.length; i++) {
			if (adj_matrix[index][i] != 0.0 && !path.contains(i)) {
				path.add(i);
				findPaths(adj_matrix, i, end_vertex, budget - adj_matrix[index][i], true);
				path.remove(i);
			}
		}

		return;
	}

    public static void main(String[] args) {
		long time = System.currentTimeMillis();

        String input = args[0];//"Testi/I_9.txt";//args[0]; 
		String output = args[1];//"rez.txt";//args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
            BufferedWriter bw = new BufferedWriter(fw);
            
			int num_of_edges = Integer.valueOf(br.readLine());
			double[][] edges_and_costs = new double[num_of_edges][3];

			HashSet<Integer> unique = new HashSet<Integer>();

			String[] line;

			for (int i = 0; i < num_of_edges; i++) {
				line = br.readLine().split(",");
				edges_and_costs[i][0] = Integer.valueOf(line[0]);
				edges_and_costs[i][1] = Integer.valueOf(line[1]);
				edges_and_costs[i][2] = Double.valueOf(line[2]);
				unique.add(Integer.valueOf(line[0]));
                unique.add(Integer.valueOf(line[1]));
			}

			line = br.readLine().split(",");
			int starting_vertex = Integer.valueOf(line[0]);
			double budget = Double.valueOf(line[1]);

			int num_of_vertexes = unique.size();
			
			double[][] adj_matrix = new double[num_of_vertexes][num_of_vertexes];
            
            for (int i = 0; i < num_of_edges; i++) {
				adj_matrix[(int)(edges_and_costs[i][1] - 1)][(int)(edges_and_costs[i][0] - 1)] = edges_and_costs[i][2];
                adj_matrix[(int)(edges_and_costs[i][0] - 1)][(int)(edges_and_costs[i][1] - 1)] = edges_and_costs[i][2];
			}

			findPaths(adj_matrix, starting_vertex - 1, starting_vertex - 1, budget, false);
			bw.write(String.valueOf(all_paths.size()));

			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();
			
			System.out.println("Time: " + (System.currentTimeMillis() - time));

		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
    }

}