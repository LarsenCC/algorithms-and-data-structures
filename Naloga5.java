import java.io.*;

class SetElement {
	Object element;
	SetElement next;

	SetElement() {
		element = null;
		next = null;
	}
}

class Set {
	private SetElement first;
	
	public Set() {
		makenull();
	}
	
	public void makenull() {
		first = new SetElement();
	}
	
	public SetElement first() {
		return first;
	}
	
	public SetElement next(SetElement pos) {
		return pos.next;
	}
	
	public void insert(Object obj) {
		// nov element vstavimo samo, ce ga ni med obstojecimi elementi mnozice
		if (locate(obj) == null) {
			SetElement nov = new SetElement();
			nov.element = obj;
			nov.next = first.next;
			first.next = nov;
		}
	}
	
	public void delete(SetElement pos) {
		pos.next = pos.next.next;
	}
	
	public boolean overEnd(SetElement pos) {
		if (pos.next == null)
			return true;
		else
			return false;
	}
	
	public boolean empty() {
		return first.next == null;
	}
	
	public Object retrieve(SetElement pos) {
		return pos.next.element;
	}
	
	public SetElement locate(Object obj) {
		// sprehodimo se cez seznam elementov in preverimo enakost (uporabimo metodo equals)
		for (SetElement iter = first(); !overEnd(iter); iter = next(iter))
			if (obj.equals(retrieve(iter)))
				return iter;
		
		return null;
	}
	
	public void print() {
		//int len = 0;
		//System.out.print("{");
		for (SetElement iter = first(); !overEnd(iter); iter = next(iter))
		{
			System.out.print(retrieve(iter));
			if (!overEnd(next(iter)))
				System.out.print(", ");
			//len++;
		}
		//System.out.println("}");
		//System.out.println("Length = " + len);
	}
}

class MapNode {	
	private Object key;
	private Object value;

	public MapNode(Object key, Object value) {
		this.key = key;
		this.value = value;
	}

	public Object getKey() {
		return key;
	}

	public Object getValue() {
		return value;
	}

	public void setKey(Object key) {
		this.key = key;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public boolean equals(Object obj) {
		if (obj instanceof MapNode)
		{
			MapNode node = (MapNode) obj;
			return key.equals(node.key);
		}
		
		return false;
	}
}

class Map {
	public static final int DEFAULT_SIZE = 7;

	Set[] table;
	
	public Map() {
		makenull(DEFAULT_SIZE);
	}
	
	public Map(int size) {
		makenull(size);
	}

	public void makenull() {
		makenull(DEFAULT_SIZE);
	}
	
	public void makenull(int size) {
		// create an empty table and initialize the linked lists
		table = new Set[size];
		
		for (int i = 0; i < table.length; i++) {
			table[i] = new Set();
		}
	}

	private int hash(Object d) {
		return Math.abs(d.hashCode()) % table.length;
	}

	public void print() {
		for (int i = 0; i < table.length; i++)
			table[i].print();
	}
	
	// Iz objekta naredi string ... ki se potem hasha
	public String stringifyCart(Object d) {
		Cart c = (Cart) d;
		String hashcode = "";

		for (int i = 1; i < c.lines.length; i++) {
			for (int j = 0; j < c.lines[i].length; j++) {
				//if (c.lines[i][j] != ' ') {
					hashcode = hashcode + c.lines[i][j];
				/*} else {
					hashcode = hashcode + c.pos;
				}*/
			}
		}
		
		return hashcode + String.valueOf(c.pos) + String.valueOf(c.containing);
	}

	public void assign(Object d, Object r) {
		// Funkcija doda nov par (d, r) v preslikavo M.
		// To pomeni, da velja M(d) = r.
		//
		// V primeru, da v preslikavi M ze obstaja par s kljucem d,
		// se obstojeci shranjeni par spremeni v (d, r).
		d = stringifyCart(d);

		Set l = table[hash(d)];
		MapNode n = new MapNode(d, r);
		SetElement pos = l.locate(n);
		if (pos != null) {
			((MapNode) l.retrieve(pos)).setValue(r);
		} else {
			l.insert(n);
		}
	
	}

	public Object compute(Object d) {
		// Funkcija vrne vrednost M(d).
		// Ce vrednost M(d) ni definirana, funkcija vrne null.
		d = stringifyCart(d);

		Set l = table[hash(d)];
		SetElement pos = l.locate(new MapNode(d, null));
		if (pos != null) {
			return ((MapNode) l.retrieve(pos)).getValue();
		} 
		
		return null;
		
	}

	public void delete(Object d) {
		// Funkcija zbrise par (d, r) iz preslikave M.
		// To pomeni, da vrednost M(d) ni vec definirana.
		d = stringifyCart(d);

		Set l = table[hash(d)];
		SetElement pos = l.locate(new MapNode(d, null));
		if (pos != null) {
			l.delete(pos);
		}
	
	}
}

class QueueElement {
	Object element;
	QueueElement next;

	QueueElement() {
		element = null;
		next = null;
	}
}

class Queue {
	private QueueElement front;
	private QueueElement rear;
	
	public Queue() {
		makenull();
	}
	
	public void makenull() {
		front = null;
		rear = null;
	}
	
	public boolean empty() {
		return (front == null);
	}
	
	public Object front() {
		if (!empty())
			return front.element;
		else
			return null;
	}
	
	public void enqueue(Object obj) {
		QueueElement el = new QueueElement();
		el.element = obj;
		el.next = null;
		
		if (empty()) {
			front = el;
		} else {
			rear.next = el;
		}
		
		rear = el;
	}
	
	public void dequeue() {
		if (!empty()) {
			front = front.next;
			
			if (front == null)
				rear = null;
		}
	}
}

class Cart {
	int pos;
	char containing;
	String moves;
	char[][] lines;
	String last_move;

	public Cart(int pos, char containing, String move, char[][] lines, String last_move) {
		this.pos = pos;
		this.containing = containing;
		this.moves = move;
		this.lines = lines;
		this.last_move = last_move;
	}

	public void printCartMoves(BufferedWriter bw) {
		String[] m = this.moves.split(":");
		for (int i = 1; i < m.length; i++) {
			try {
				bw.write(m[i] + "\n");
			} catch (IOException e) {}
		}
	}
}

public class Naloga5 {

	static String final_chars = "";

	public static boolean isInFinal(char element) {
		return final_chars.contains(String.valueOf(element));
	}

	public static boolean checkEnd(Cart c, char[][] final_lines, BufferedWriter bw) {
		boolean done = true;
		for (int i = 1; i < c.lines.length; i++) {
			for (int j = 0; j < c.lines[i].length; j++) {
				if (final_lines[i][j] != c.lines[i][j]) {
					done = false;
					break;
				}
			}

			if (!done) {
				break;
			}
		}

		if (done) {
			/*String[] result = c.moves.split(":");
			for (int i = 1; i < result.length; i++) {
				System.out.println(result[i]);
			}*/
			//map.print();
			c.printCartMoves(bw);
			return true;
		}

		return false;
	}

	public static boolean isEmpty(char[] list) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] != ' ') {
				return false;
			}
		}

		return true;
	}

    public static void main(String[] args) {
		//long start_time = System.currentTimeMillis();

        String input = args[0];//args[0]; "Testi/I5_10.txt";
		String output = args[1];//args[1]; "rez.txt";
        
        int num_of_lines, max_objects_on_line; // N and P

		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);
			
			String[] line;

            line = br.readLine().split(",");
			num_of_lines = Integer.valueOf(line[0]);
			max_objects_on_line = Integer.valueOf(line[1]);

			// num_of_lines + 1, so that we dont have to deal with indexes ...
			char[][] lines = new char[num_of_lines + 1][max_objects_on_line];
			char[][] final_lines = new char[num_of_lines + 1][max_objects_on_line];

			// READ FILE
			for (int i = 1; i < lines.length; i++) {
				for (int j = 0; j < max_objects_on_line; j++) {
					lines[i][j] = ' ';
					final_lines[i][j] = ' ';
				}
			}

			for (int e = 0; e < 2; e++) {
				for (int i = 1; i < lines.length; i++) {
					line = br.readLine().split(":");

					try {
						line = line[1].split(",");
					} catch (IndexOutOfBoundsException ex) {
						line = null;
					}

					for (int j = 0; j < max_objects_on_line; j++) {
						try {
							if (e == 0) {
								lines[i][j] = line[j].charAt(0);
							} else {
								final_lines[i][j] = line[j].charAt(0);
								if (line[j].charAt(0) != ' ') {
									final_chars += line[j].charAt(0);
								}
							}
							
						} catch (StringIndexOutOfBoundsException ex) {
						} catch (Exception ex) {
							break;
						}
					}
				}
			}
			//System.out.println("Final elements ... " + final_chars);

			//System.out.println("Time to read = " + (System.currentTimeMillis() - start_time) + " ms");

			// START ALOGRITHM
			Cart c = new Cart(1, ' ', "START", lines, "START");
			Queue queue = new Queue();
			queue.enqueue(c);
			Map map = new Map(131071); //131071   236461
			
			if (checkEnd(c, final_lines, bw)) {
				queue.dequeue();
			}

			//printAll(c, max_objects_on_line);

			while(!queue.empty()) {
				c = (Cart) queue.front();
				queue.dequeue();

				if (map.compute(c) != null) {
					//System.out.println("Continue");
					continue;
				} else {
					map.assign(c, 1);
				}

				//printAll(c, max_objects_on_line);
				
				// Not done? Look deeper ...
				char[][] temp;
				Cart c1;

				boolean breakLoop = false;

				// PREMIK
				for (int i = 1; i <= num_of_lines; i++) {
					if (!c.last_move.equals("PREMIK") && c.pos != i && (c.containing != ' ' || !isEmpty(c.lines[i]))) {
						String premik = "PREMIK " + i;
						temp = c.lines.clone();
						for (int k = 0; k < c.lines.length; k++) {
							temp[k] = c.lines[k].clone();
						}
						c1 = new Cart(i, c.containing, c.moves + ":" + premik, temp, "PREMIK");
						queue.enqueue(c1);
						//map.assign(c1, 1);
					}
				}
				
				// NALOZI
				if (c.lines[c.pos][0] != ' ' && c.containing == ' ') {
					temp = c.lines.clone();
					for (int i = 0; i < c.lines.length; i++) {
						temp[i] = c.lines[i].clone();
					}
					char add = c.lines[c.pos][0];
					temp[c.pos][0] = ' ';
					c1 = new Cart(c.pos, add, c.moves + ":" + "NALOZI", temp, "NALOZI");
					breakLoop = checkEnd(c1, final_lines, bw);
					if (breakLoop) break;
					queue.enqueue(c1);
					//map.assign(c1, 1);
				}

				if (breakLoop) break;

				// ODLOZI
				if (c.containing != ' ' && c.lines[c.pos][0] == ' ') {
					temp = c.lines.clone();
					for (int i = 0; i < c.lines.length; i++) {
						temp[i] = c.lines[i].clone();
					}
					temp[c.pos][0] = c.containing;
					c1 = new Cart(c.pos, ' ', c.moves + ":" + "ODLOZI", temp, "ODLOZI");
					breakLoop = checkEnd(c1, final_lines, bw);
					if (breakLoop) break;
					queue.enqueue(c1);
					//map.assign(c1, 1);
				}

				if (breakLoop) break;

				// GOR
				int len = c.lines[c.pos].length;
				char el = c.lines[c.pos][len - 1];
				boolean check = el == ' ';

				if (!isEmpty(c.lines[c.pos]) && (check || !isInFinal(el))) { //!isInFinal(final_lines, el)
					temp = c.lines.clone();
					for (int i = 0; i < c.lines.length; i++) {
						temp[i] = c.lines[i].clone();
					}
					for (int i = c.lines[c.pos].length - 1; i >= 0; i--) {
						if (i == c.lines[c.pos].length - 1) {
							temp[c.pos][i] = ' ';
						} else {
							char current = temp[c.pos][i];
							temp[c.pos][i] = ' ';
							temp[c.pos][i + 1] = current;
						}
					}
					c1 = new Cart(c.pos, c.containing, c.moves + ":" + "GOR", temp, "GOR");
					breakLoop = checkEnd(c1, final_lines, bw);
					if (breakLoop) break;
					queue.enqueue(c1);
				}
				//map.assign(c1, 1);
				
				if (breakLoop) break;

				// DOL
				el = c.lines[c.pos][0];
				check = el == ' ';

				if (!isEmpty(c.lines[c.pos]) && (check || !isInFinal(el))) { //!isInFinal(final_lines, el)
					temp = c.lines.clone();
					for (int i = 0; i < c.lines.length; i++) {
						temp[i] = c.lines[i].clone();
					}
					for (int i = 0; i < c.lines[c.pos].length; i++) {
						if (i == 0) {
							temp[c.pos][i] = ' ';
						} else {
							char current = temp[c.pos][i];
							temp[c.pos][i] = ' ';
							temp[c.pos][i - 1] = current;
						}
					}
					c1 = new Cart(c.pos, c.containing, c.moves + ":" + "DOL", temp, "DOL");
					breakLoop = checkEnd(c1, final_lines, bw);
					if (breakLoop) break;
					queue.enqueue(c1);
				}

				if (breakLoop) break;

				//map.assign(c1, 1);

				//System.out.println("Time for queue = " + (System.nanoTime() - time_queue) + " ns");
			}

			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();

			//System.out.println("Time: " + (System.currentTimeMillis() - start_time));
			
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading/writing file");
        }
    }

}