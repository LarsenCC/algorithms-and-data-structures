import java.io.*;
import java.util.*;

public class Naloga6 {

    static int red = 0;
    static int blue = 1;

    public static int colorGraph(int[][] adj_matrix, int index, String[][] unique_color) {
        unique_color[index][2] = null;

        if ((red + blue) == unique_color.length) {
            return 0;
        }

        int res = 0;

        for (int i = 0; i < adj_matrix.length; i++) {
            if (adj_matrix[index][i] == 1 && unique_color[i][2] != null) {
                if (unique_color[i][1] == null) {
                    //unique_color[i][1] = unique_color[index][1].equals("b") ? "r" : "b";
                    if (unique_color[index][1].equals("b")) {
                        unique_color[i][1] = "r";
                        red++;
                    } else {
                        unique_color[i][1] = "b";
                        blue++;
                    }

                    res = colorGraph(adj_matrix, i, unique_color);
                    if (res == -1) {
                        return -1;
                    }
                }               
            } else if (adj_matrix[index][i] == 1 && unique_color[i][2] == null) {
                if (unique_color[index][1].equals(unique_color[i][1])) {
                    return -1;
                }
            }
        }

        return res;
    }

    public static void main(String[] args) {
        long time = System.currentTimeMillis();

        String input = args[0]; //"Testi/I_3.txt";//args[0]; 
		String output = args[1]; //args[1];
		
		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);
			BufferedWriter bw = new BufferedWriter(fw);
            
            HashSet<String> unique = new HashSet<String>();
            HashMap<String,Integer> unique_indexes = new HashMap<String,Integer>();

            int lines = Integer.valueOf(br.readLine());
            String[][] edges = new String[lines][2];

            for (int i = 0; i < lines; i++) {
                String[] line = br.readLine().split("-");
                edges[i][0] = line[0];
                edges[i][1] = line[1];
                unique.add(line[0]);
                unique.add(line[1]);
            }

            String[][] unique_color = new String[unique.size()][3];
            int index = 0;

            Iterator<String> iter = unique.iterator();
            while(iter.hasNext()) {
                String next = iter.next();
                unique_color[index][0] = next;
                unique_color[index][2] = next;
                unique_indexes.put(next, index);
                index++;
            }

            int[][] adj_matrix = new int[unique_color.length][unique_color.length];
            
            for (int i = 0; i < lines; i++) {
                adj_matrix[unique_indexes.get(edges[i][0])][unique_indexes.get(edges[i][1])] = 1;
                adj_matrix[unique_indexes.get(edges[i][1])][unique_indexes.get(edges[i][0])] = 1;
            }

            // COLOR THE GRAPH
            unique_color[0][1] = "b";
            
            int res = colorGraph(adj_matrix, 0, unique_color);

            if ((res == -1) || (red == blue)) {
                bw.write("-1");
                //System.out.println("-1");
            } else {
                String compare = red > blue ? "r" : "b";
                for (int i = 0; i < unique_color.length; i++){
                    if (unique_color[i][1].equals(compare)) {
                        bw.write(unique_color[i][0] + "\n");
                        //System.out.println(unique_color[i][0]);
                    }
                }
            }
			// Always close the bufferedReader and bufferedWriter!
			br.close();
            bw.close();

            System.out.println("Time: " + (System.currentTimeMillis() - time));
            
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
    }
}