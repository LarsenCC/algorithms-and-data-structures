import java.io.*;

class LinkedListElement {
	Object element;
	LinkedListElement next;
	
	LinkedListElement(Object obj) {
		element = obj;
		next = null;
	}
	
	LinkedListElement(Object obj, LinkedListElement nxt) {
		element = obj;
		next = nxt;
	}
}

class LinkedList {
	protected LinkedListElement first;
	protected LinkedListElement last;
	
	LinkedList() {
		makenull();
	}
	
	//Funkcija makenull inicializira seznam
	public void makenull() {
		//drzimo se implementacije iz ucbenika:
		//po dogovoru je na zacetku glava seznama (header)
		first = new LinkedListElement(null, null);
		last = null;
	}

	public LinkedListElement first() {
		return first;
	}
	
	public LinkedListElement next(LinkedListElement pos) {
		return pos.next;
	}

	public boolean overEnd(LinkedListElement pos) {
		if (pos.next == null)
			return true;
		else
			return false;
	}

	public Object retrieve(LinkedListElement pos) {
		return pos.next.element;
	}

	public LinkedListElement locate(Object obj) {
		// sprehodimo se cez seznam elementov in preverimo enakost (uporabimo metodo equals)
		for (LinkedListElement iter = first(); !overEnd(iter); iter = next(iter)) {
			if (obj.equals(retrieve(iter))) {
				return iter;
			}
		}
		return null;
	}

	//Funkcija addLast doda nov element na konec seznama
	public void addLast(Object obj) {
		//najprej naredimo nov element
		LinkedListElement newEl = new LinkedListElement(obj, null);
		
		//ali je seznam prazen?
		// po dogovoru velja: ce je seznam prazen, potem kazalec "last" ne kaze nikamor
		if (last == null) {
			//ce seznam vsebuje samo en element, kazalca "first" in "last" kazeta na glavo seznama
			first.next = newEl;
			last = first;
		} else {
			last.next.next = newEl;
			last = last.next;
		}
	}
	
	//Funkcija write izpise elemente seznama
	/*public void write() {
		for (LinkedListElement iter = first(); !overEnd(iter); iter = next(iter)) {
			System.out.print(retrieve(iter));
			if (!overEnd(next(iter))) {
				System.out.print(",");
			}
		}
		System.out.println();
	}*/

	//Funkcija write izpise elemente seznama v file
	public void write(BufferedWriter bw) {
		try {
			for (LinkedListElement iter = first(); !overEnd(iter); iter = next(iter)) {
				bw.write(String.valueOf(retrieve(iter)));
				if (!overEnd(next(iter))) {
					bw.write(",");
				}
			}
			bw.write("\n");
		} catch (IOException ex) {
			System.out.println("Something went wrong while writing file");
        }
		
	}
	
	//Funkcija addFirst doda nov element na prvo mesto v seznamu (takoj za glavo seznama)
	void addFirst(Object obj) {
		//najprej naredimo nov element
		LinkedListElement newEl = new LinkedListElement(obj);
		
		//novi element postavimo za glavo seznama
		newEl.next = first.next;
		first.next = newEl;
		
		if (last == null)//preverimo, ali je to edini element v seznamu
			last = first;
		else if (last == first)//preverimo, ali je seznam vseboval en sam element
			last = newEl;
	}
}

public class Naloga2 {
    public static void main(String[] args) {
        //long time = System.currentTimeMillis(); // current time

		String input = args[0]; //"Testi/I2_4.txt";//args[0]; 
		String output = args[1]; //"rez.txt";//args[1];

		// Read input data
		try {
			// Create File and Buffered reader for input files
			FileReader fr = new FileReader(input);
            BufferedReader br = new BufferedReader(fr);
            
            // Create File and Buffered writer for output files
            FileWriter fw = new FileWriter(output);	
			BufferedWriter bw = new BufferedWriter(fw);

			String[] line;
			
			LinkedList deck = new LinkedList();
			
			//------------------- Read file --------------
			line = br.readLine().split(",");
			int num_of_cards = Integer.valueOf(line[0]);
			int num_of_shuffles = Integer.valueOf(line[1]);

			line = br.readLine().split(",");
			for (int i = 0; i < num_of_cards; i++) {
				deck.addLast(line[i]);
			}

			//deck.write();

			LinkedListElement cut = null; // karta, ki razdeli kup (zadnja v K1) --> D
			LinkedListElement put = null; // karta za katero se v K1 zacnejo vstavlati karte iz K2 --> V
			int cards_per_iter = 0; // stevilo kart, ki se postavi za put na iteracijo --> S

			int num_k1 = 0; // stevilo kart v K1
			int num_k2 = 0; // stevilo kart v K2

			for (int i = 0; i < num_of_shuffles; i++) {
				num_k1 = 0;
				num_k2 = 0;

				// read the file line
				line = br.readLine().split(",");
				cards_per_iter = Integer.valueOf(line[2]);

				// set the booleans of found to false
				boolean found_cut = false;
				boolean found_put = false;

				// set cut and put to null ... we havent found any cards yet ...
				cut = null;
				put = null;

				for (LinkedListElement iter = deck.first(); !deck.overEnd(iter); iter = deck.next(iter)) {
					num_k1++;

					// check if iter equals V ...
					if (!found_put && String.valueOf(deck.retrieve(iter)).equals(line[1])) {
						put = iter;
						found_put = true;
					}

					// check if iter equals D ...
					if (String.valueOf(deck.retrieve(iter)).equals(line[0])) {
						cut = iter;
						found_cut = true;
						num_k2 = num_of_cards - num_k1;
						break;
					}
				}

				// if we find a cut card ... then fine ...
				int num_of_iters = num_k2 / cards_per_iter;
				int last_iter = num_k2 % cards_per_iter;
				//LinkedListElement k1 = new LinkedListElement(null, deck.first().next);
				LinkedListElement k2;
				if (found_cut) {
					if (cut.next.next != null) {
						k2 = new LinkedListElement(cut.next.next.element, cut.next.next.next); //direktno element
					} else {
						k2 = null;
					}
				} else {
					k2 = new LinkedListElement(deck.first.next.element, deck.first.next.next); //direktno element
					put = new LinkedListElement(null, deck.first);
					num_of_iters = num_of_cards / cards_per_iter;
					last_iter = num_of_cards % cards_per_iter;
				}

				if (found_cut && !found_put) {
					put = new LinkedListElement(null, deck.first);
				}

				LinkedListElement shuffle = k2;
				LinkedListElement shuffle_last = k2;
				if (put != cut && found_cut) {
					cut.next.next = null;
				} else {
					int it = num_of_iters == 0 ? last_iter : cards_per_iter;
					for (int k = 1; k < it; k++) {
						shuffle = shuffle.next;
					}
					LinkedListElement temp = shuffle.next;
					shuffle.next = null;
					shuffle = temp;
					shuffle_last = temp;
					num_of_iters--; // ker je ze prva iteracija izvedena
				}

				//deck.write();
				// place the first few packs in the K1 deck
				for (int k = 0; k <= num_of_iters; k++) {
					boolean ok = false;
					if (last_iter != 0 && k == num_of_iters) {
						for (int j = 1; j < last_iter; j++) {
							shuffle_last = shuffle_last.next;
						}
						ok = true;
					} else if (k != num_of_iters) {
						for (int j = 1; j < cards_per_iter; j++) {
							shuffle_last = shuffle_last.next;
						}
						ok = true;
					}
					
					//deck.write();
					if (ok) {
						LinkedListElement temp = shuffle_last.next;
						shuffle_last.next = put.next.next;
						put.next.next = shuffle;
						shuffle = temp;
						shuffle_last = temp;
						//deck.write();
					}
				}
			}

			//deck.write();
			deck.write(bw);
			//System.out.println("Time: " + (System.currentTimeMillis() - time) + " ms");
			// Always close the bufferedReader and bufferedWriter!
			br.close();
			bw.close();
            
		// Catch exceptions
		} catch (FileNotFoundException ex) {
			System.out.println("Unable to open file '" + input + "'");
		} catch (IOException ex) {
			System.out.println("Something went wrong while reading file");
        }
        
	}
}